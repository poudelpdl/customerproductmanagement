$(function(){

   searchProduct();

});

var searchProduct = function () {

    $( ".searchProduct" ).click(function() {
        $.ajax({
            url: '/search/searchProduct',
            data: {id: $('#product').val()},
            success: function (data) {
                $('#search-result-container').html('');
                if(data){
                    $('#search-result-container').html(data);
                }

            },
            error: function () {
                alert("There was error in search");
                location.reload();
            }
        });
    });

};