package customerProductManagement

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

       // 'search/productSearch'(controller: 'search' , action: 'productSearch')

        "/"(controller: 'homePage', action: "index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
