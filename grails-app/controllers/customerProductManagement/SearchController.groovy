package customerProductManagement


class SearchController {

    def searchProduct() {

        if (!params.id) {
            render ""
            return
        }
        Product product = Product.findById(params.id as Long)
        Customer customer = Customer.findByProduct(product)
        if(!product){
            render ""
            return
        }
        render (template: "/searchResult",
                model: [product: product,customer:customer])

    }
}
