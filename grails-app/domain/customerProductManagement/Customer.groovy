package customerProductManagement

class Customer {
    String firstName
    String lastName
    String phoneNumber
    String  emailAddress
    Product product

    static constraints = {
        firstName nullable: false ,blank:false
        lastName nullable: true
        phoneNumber nullable: false ,blank:false  ,validator: { value ->
            if(!value.isInteger()) { "Invalid value" }
        }
        emailAddress nullable: false ,blank:false , email: true
    }

    public String getFullname() {
        return "${firstName} ${lastName}"
    }
}

