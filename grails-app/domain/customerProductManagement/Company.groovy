package customerProductManagement

class Company {
    String name
    String phoneNumber
    String addressOne
    String addressTwo
    String suburb
    String postcode
    String state



    static constraints = {
        name nullable: true , blank:true
        phoneNumber nullable: true , blank:true
        addressOne nullable: true , blank:true
        addressTwo nullable: true , blank:true
        suburb nullable: true ,blank:true
        postcode nullable: true , blank:true
        state nullable: true  , blank:true

    }
}
