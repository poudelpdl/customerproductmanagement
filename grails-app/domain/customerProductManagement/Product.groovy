package customerProductManagement

class Product {
    String name
    Date manufactureDate

     static belongsTo = [company:Company]

    static constraints = {
        name blank: false, nullable: false, maxSize: 255
        manufactureDate nullable: false ,validator: { value ->
            if(value.after(new Date())) { "Invalid Production Date" }
        }
    }

    public String setCompany(){
        return this.company.name
    }
}
