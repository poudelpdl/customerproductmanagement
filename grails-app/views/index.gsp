<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

    <div class="svg" role="presentation">
        <div class="grails-logo-container">
          %{--  <asset:image src="hero-image.jpg" class="grails-logo"/>--}%
        </div>
    </div>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Welcome to Customer Product  Management</h1>

            <div class="col-md-4  top30">
                <g:link controller="company"  action="index">Company List</g:link>
            </div>
            <div class="col-md-4 top30">
                <g:link controller="product"  action="index">Product List</g:link>
            </div>
            <div class="col-md-4 top30">
                <g:link controller="customer"  action="index">Customer List</g:link>
            </div>
        </section>

        <section class="row colset-2-its top30" id="search-result-container">

        </section>



    </div>


</body>
</html>
