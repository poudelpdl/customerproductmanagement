import customerProductManagement.Company
import customerProductManagement.Customer
import customerProductManagement.Product

class BootStrap {

    def init = { servletContext ->

        if (Company.count() == 0) {
            new Company(name: 'Dell', phoneNumber: '123456', addressOne: 'PittStreet', addressTwo: '', suburb: 'Sydney', postcode: 20, state: 'NSW').save()
            new Company(name: 'Apple', phoneNumber: '123456', addressOne: 'PittStreet', addressTwo: '', suburb: 'Sydney', postcode: 20, state: 'NSW').save()
            new Company(name: 'Samsung', phoneNumber: '123456', addressOne: 'PittStreet', addressTwo: '', suburb: 'Sydney', postcode: 20, state: 'NSW').save()

        }

        if (Product.count() == 0) {
            new Product(name: 'laptop', manufactureDate: new Date(),ompany: Company.findByName('Dell')).save()
            new Product(name: 'Mobile', manufactureDate: new Date(),company: Company.findByName('Apple')).save()
            new Product(name: 'Watch', manufactureDate: new Date(),company: Company.findByName('Samsung')).save()
        }

        if (Customer.count() == 0) {
            new Customer(firstName: 'Prabin', lastName: 'Poudel',phoneNumber: '042777777777',emailAddress: 'test@test.com',product: Product.findByName('Mobile')).save()
        }

    }
    def destroy = {
    }
}
